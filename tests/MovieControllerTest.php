<?php

namespace App\Test\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MovieControllerTest extends WebTestCase
{
  public function testHomepageIsSuccessful(): void
  {
    $client = self::createClient();
    $client->request('GET', '/movie');
    self::assertResponseIsSuccessful();
  }
}