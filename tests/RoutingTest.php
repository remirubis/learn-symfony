<?php

namespace App\Test\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class RoutingTest extends WebTestCase
{
  public function provideUrisWithStatusCodes(): \Generator
  {
    // yield ['/', Response::HTTP_OK];
    yield ['/movie', Response::HTTP_OK];
    yield ['/book', Response::HTTP_OK];
  }

  /**
   * @dataProvider provideUrisWithStatusCodes
   */
  public function testApplicationRoutes(string $uri, int $exceptedStatusCode): void
  {
    $client = static::createClient();
    $client->request(Request::METHOD_GET, $uri);
    self::assertResponseStatusCodeSame($exceptedStatusCode);
  }
}